package com.pos292.EmployeeLeave.Controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.pos292.EmployeeLeave.Model.Biodata;
import com.pos292.EmployeeLeave.Model.Employee;
import com.pos292.EmployeeLeave.Repo.BiodataRepo;
import com.pos292.EmployeeLeave.Repo.EmployeeRepo;



@RestController
@CrossOrigin("*")
@RequestMapping
public class ApiEmployeeController {

	@Autowired
	private EmployeeRepo employeeRepo;
	
	@Autowired
	private BiodataRepo biodataRepo;
	
	public ApiEmployeeController(EmployeeRepo employeeRepo) {
		super();
		this.employeeRepo = employeeRepo;
	}
	
	// get employee by biodata id
	@GetMapping("/api/employeebybiodata/{id}")
    public ResponseEntity<List<Employee>> GetAllEmployeeByBiodataId(@PathVariable("id") Long id)
    {
        try
        {
            List<Employee> employee = employeeRepo.FindByBiodataId(id);
            return new ResponseEntity<>(employee, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
	
	// get all data biodata
	@GetMapping(value = "/api/biodata")
	public ResponseEntity<List<Biodata>> GetAllBiodata(){ 
		try {
			List<Biodata> biodata = this.biodataRepo.findAll();
			return new ResponseEntity<>(biodata, HttpStatus.OK);
		}
		catch(Exception exception) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	// get all data employee
	@GetMapping(value = "/api/employee")
	public ResponseEntity<List<Employee>> GetAllEmployee(){ 
		try {
			List<Employee> employee = employeeRepo.findAll();
			return new ResponseEntity<>(employee, HttpStatus.OK);
		}
		catch(Exception exception) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	// pageable 
	@GetMapping("/api/employeemapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<Employee> employee = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Employee> pageTuts;

            pageTuts = employeeRepo.findAll(pagingSort);

            employee = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("employee", employee);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
	
	// post employee untuk create 
	@PostMapping("/api/employee")
	public ResponseEntity<Object> SaveEmployee(@RequestBody Employee employee){
		try {
			employeeRepo.save(employee);
			return new ResponseEntity<>(employee,HttpStatus.OK);
		}
		catch(Exception exception) {
			return new ResponseEntity<>("Failed",HttpStatus.BAD_REQUEST);
		}
	}
	
	// get employee by id
	@GetMapping("api/employee/{id}")
	public ResponseEntity<List<Employee>> GetEmployeeById(@PathVariable("id") Long id){
		try {
			Optional<Employee> employee = employeeRepo.findById(id);
			
			if(employee.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(employee, HttpStatus.OK);
				return rest;
			}else {
				return ResponseEntity.notFound().build();
			}
		}
		catch (Exception exception) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	// put untuk update data employee
	@PutMapping("api/employee/{id}")
	public ResponseEntity<Object> UpdateEmployee(@RequestBody Employee employee, @PathVariable("id") Long id){
		Optional<Employee> employeeData = employeeRepo.findById(id);
		if(employeeData.isPresent()) {
			employee.setId(id);
			employeeRepo.save(employee);
			ResponseEntity rest = new ResponseEntity<>("Success",HttpStatus.OK);
			return rest;
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	// delete data employee by id
	@DeleteMapping("api/employee/{id}")
	public ResponseEntity<Object> DeleteEmployee(@PathVariable("id") Long id){
		Optional<Employee> employeeData = employeeRepo.findById(id);
		if(employeeData.isPresent()) {
			employeeRepo.deleteById(id);
			ResponseEntity rest = new ResponseEntity<>("Success",HttpStatus.OK);
			return rest;
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
}
