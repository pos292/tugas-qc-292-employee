package com.pos292.EmployeeLeave.Controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pos292.EmployeeLeave.Model.Biodata;
import com.pos292.EmployeeLeave.Model.Employee;
import com.pos292.EmployeeLeave.Model.EmployeeLeave;
import com.pos292.EmployeeLeave.Repo.EmployeeLeaveRepo;

@RestController
@CrossOrigin("*")
@RequestMapping
public class ApiEmployeeLeaveController {

	@Autowired
	private EmployeeLeaveRepo employeeLeaveRepo;
	
	// pageable 
	@GetMapping("/api/employeeleavemapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
		try {
            List<EmployeeLeave> employeeleave = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<EmployeeLeave> pageTuts;

            pageTuts = employeeLeaveRepo.findAll(pagingSort);

            employeeleave = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("employeeleave", employeeleave);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
	
	// post employee leave untuk create 
	@PostMapping("/api/employeeleave")
	public ResponseEntity<Object> SaveEmployeeLeave(@RequestBody EmployeeLeave employeeleave){
		try {
			employeeLeaveRepo.save(employeeleave);
			return new ResponseEntity<>(employeeleave,HttpStatus.OK);
		}
		catch(Exception exception) {
			return new ResponseEntity<>("Failed",HttpStatus.BAD_REQUEST);
		}
	}
	
	
		// put untuk update data employee leave
		@PutMapping("api/employeeleave/{id}")
		public ResponseEntity<Object> UpdateEmployeeLeave(@RequestBody EmployeeLeave employeeleave, @PathVariable("id") Long id){
			Optional<EmployeeLeave> employeeLeaveData = employeeLeaveRepo.findById(id);
			if(employeeLeaveData.isPresent()) {
				employeeleave.setId(id);
				employeeLeaveRepo.save(employeeleave);
				ResponseEntity rest = new ResponseEntity<>("Success",HttpStatus.OK);
				return rest;
			}else {
				return ResponseEntity.notFound().build();
			}
		}
		
		
		// get employee leave by id
		@GetMapping("api/employeeleave/{id}")
		public ResponseEntity<List<EmployeeLeave>> GetEmployeeLeaveById(@PathVariable("id") Long id){
			try {
				Optional<EmployeeLeave> employeeleave = employeeLeaveRepo.findById(id);
				
				if(employeeleave.isPresent()) {
					ResponseEntity rest = new ResponseEntity<>(employeeleave, HttpStatus.OK);
					return rest;
				}else {
					return ResponseEntity.notFound().build();
				}
			}
			catch (Exception exception) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		}
		
		// delete data employee leave by id
		@DeleteMapping("api/employeeleave/{id}")
		public ResponseEntity<Object> DeleteEmployeeLeave(@PathVariable("id") Long id){
			Optional<EmployeeLeave> employeeLeaveData = employeeLeaveRepo.findById(id);
			if(employeeLeaveData.isPresent()) {
				employeeLeaveRepo.deleteById(id);
				ResponseEntity rest = new ResponseEntity<>("Success",HttpStatus.OK);
				return rest;
			}else {
				return ResponseEntity.notFound().build();
			}
		}

}
