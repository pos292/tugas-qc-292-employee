package com.pos292.EmployeeLeave.Model;

import javax.persistence.*;

import org.hibernate.annotations.Where;

@Entity
@Table(name = "biodata")
public class Biodata {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private long Id;
	
	@Column(name = "first_name", nullable = false)
	private String FirstName;
	
	@Column(name = "last_name", nullable = false)
	private String LastName;
	
	@Column(name = "dob", nullable = false)
	private String dob;
	
	@Column(name = "pob", nullable = false)
	private String pob;
	
	@Column(name = "address", nullable = false)
	private String Address;
	
	@Column(name = "marital_status", nullable = false)
	private Boolean MaritalStatus;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getPob() {
		return pob;
	}

	public void setPob(String pob) {
		this.pob = pob;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public Boolean getMaritalStatus() {
		return MaritalStatus;
	}

	public void setMaritalStatus(Boolean maritalStatus) {
		MaritalStatus = maritalStatus;
	}
	
}
