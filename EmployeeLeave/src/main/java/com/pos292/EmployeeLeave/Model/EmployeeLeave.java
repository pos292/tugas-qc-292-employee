package com.pos292.EmployeeLeave.Model;

import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "employee_leave")
@SQLDelete(sql = "UPDATE employee_leave SET is_delete = true WHERE id = ?")
@Where(clause = "is_delete = false")
public class EmployeeLeave {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="id", nullable = false)
	private Long Id;
	
	@Column(name = "period",nullable = false)
	private String Period;
	
	@Column(name = "reguler_quota",nullable = false)
	private Integer RegulerQuota;
	
	@Column(name = "employee_id", nullable = false)
	private Long EmployeeId;
	
	@ManyToOne
	@JoinColumn(name = "employee_id", insertable = false, updatable = false)
	public Employee employee;
	
    @Column(name = "is_delete", nullable = false)
    private Boolean IsDelete = false;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getPeriod() {
		return Period;
	}

	public void setPeriod(String period) {
		Period = period;
	}

	public Integer getRegulerQuota() {
		return RegulerQuota;
	}

	public void setRegulerQuota(Integer regulerQuota) {
		RegulerQuota = regulerQuota;
	}

	public Long getEmployeeId() {
		return EmployeeId;
	}

	public void setEmployeeId(Long employeeId) {
		EmployeeId = employeeId;
	}
	
}
