package com.pos292.EmployeeLeave.Model;

import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.beans.factory.annotation.Value;


@Entity
@Table(name = "employee")
@SQLDelete(sql = "UPDATE employee SET is_delete = true WHERE id = ?")
@Where(clause = "is_delete = false")
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long Id;
	
	@Column(name = "nip", nullable = false)
	private String Nip;
	
	@Column(name = "status", nullable = false)
	private String Status;
	
	@Column(name = "salary", nullable = false)
	private Integer salary;
	
	@Column(name = "biodata_id", nullable = true)
    private Long BiodataId;
    
    @OneToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    public Biodata biodata;
    
    @Column(name = "is_delete", nullable = false)
    private Boolean IsDelete = false;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getNip() {
		return Nip;
	}

	public void setNip(String nip) {
		Nip = nip;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public Integer getSalary() {
		return salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public Long getBiodataId() {
		return BiodataId;
	}

	public void setBiodataId(Long biodataId) {
		BiodataId = biodataId;
	}

	public Boolean getIsDelete() {
		return IsDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		IsDelete = isDelete;
	}

    
}
