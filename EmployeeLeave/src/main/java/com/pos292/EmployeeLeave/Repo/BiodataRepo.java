package com.pos292.EmployeeLeave.Repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pos292.EmployeeLeave.Model.Biodata;

public interface BiodataRepo extends JpaRepository<Biodata, Long>{
}
