package com.pos292.EmployeeLeave.Repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.pos292.EmployeeLeave.Model.EmployeeLeave;

public interface EmployeeLeaveRepo extends JpaRepository<EmployeeLeave, Long> {
}
